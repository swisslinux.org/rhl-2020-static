RHL 2020, static copy of the website
====================================

This is a static copy of the website of the 2020 verson of Rencontres
Hivernales du Libre.



Build
-----

With Podman:
```sh
podman build -t 2020.hivernal.es:latest .
```


With Docker:
```sh
docker build -t 2020.hivernal.es:latest .
```


RUN
---

With Podman:
```sh
podman run 2020.hivernal.es:latest
```

With Docker:
```sh
docker run 2020.hivernal.es:latest
```


Author
------

Sébastien Gendre <seb@k-7.ch>
